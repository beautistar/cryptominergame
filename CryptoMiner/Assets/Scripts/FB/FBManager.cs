﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System.IO;

public class FBManager : MonoBehaviour {
	private static FBManager _instance = null;
	public static FBManager Instance {get{ return _instance;}}

	private string status = "Ready";
	private string lastResponse = string.Empty;
	public string userName;
	public bool bQueryScore = false;
	public bool bLogined = false;
	bool ShareImage = false;
	string Screenshot_Name;
	IDictionary<string, string> dic;

	void Awake(){
		_instance = this;	
	}

	// Use this for initialization
	void Start () {
		dic = new Dictionary<string, string> ();
		DontDestroyOnLoad(gameObject);
		bQueryScore = false;
		bLogined = false;
		Screenshot_Name = "Screenshot.png";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected string Status
	{
		get
		{
			return this.status;
		}

		set
		{
			this.status = value;
		}
	}

	protected string LastResponse
	{
		get
		{
			return this.lastResponse;
		}

		set
		{
			this.lastResponse = value;
		}
	}

	public void FBLogin(){	
	
		if (!FB.IsLoggedIn){
			FB.Init(this.OnInitComplete, this.OnHideUnity, null);
		}else{
			StartCoroutine(GetUserNameCoroutine(AccessToken.CurrentAccessToken.UserId));
		}
	}

	//getting the username
	IEnumerator GetUserNameCoroutine(string fb_id){
		Debug.Log("Call1");
		string quary = fb_id + "?fields=name";
		FB.API (quary, HttpMethod.GET,UserNameCallBack, dic);
		yield return 0;
	}

	//user name callback
	void UserNameCallBack (IResult result)
	{
		if (result.Error != null) {                                                                      
			Debug.Log ("Error");
		} else {
			userName = "" + result.ResultDictionary ["name"];
			Debug.Log("userName:" + userName);
			GameManager.Instance.SetRankUserName (userName);
			bLogined = true;
			bQueryScore = false;
			QueryScore ();
		}
	}

	private void OnInitComplete()
	{
		this.Status = "Success - Check log for details";
		this.LastResponse = "Success Response: OnInitComplete Called\n";
		string logMessage = string.Format(
			"OnInitCompleteCalled IsLoggedIn='{0}' IsInitialized='{1}'",
			FB.IsLoggedIn,
			FB.IsInitialized);
		Debug.Log(logMessage);
		if (AccessToken.CurrentAccessToken != null)
		{
			Debug.Log(AccessToken.CurrentAccessToken.ToString());
		}

		if(FB.IsInitialized)
			FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.HandleResult);
	}

	private void OnHideUnity(bool isGameShown)
	{
		this.Status = "Success - Check log for details";
		this.LastResponse = string.Format("Success Response: OnHideUnity Called {0}\n", isGameShown);
		Debug.Log("Is game shown: " + isGameShown);
	}

	public void FBLogout()
	{
		FB.LogOut();
		bQueryScore = false;
		bLogined = false;
	}

	public void FBAppInvite(){
		if (!FB.IsLoggedIn)
			FBLogin ();
		else {
			string appid = FB.AppId;
			this.Status = "Logged FB.AppEvent";
//			FB.Mobile.AppInvite (new Uri ("https://fb.me/"+appid),null, this.HandleResult);
			FB.Mobile.AppInvite (new Uri ("https://fb.me/134763387272050"),null, this.HandleResult);
		}
	}

	protected void HandleResult(IResult result)
	{
		if (result == null)
		{
			this.LastResponse = "Null Response\n";
//			LogView.AddLog(this.LastResponse);
			return;
		}

//		this.LastResponseTexture = null;

		// Some platforms return the empty string instead of null.
		if (!string.IsNullOrEmpty(result.Error))
		{
			this.Status = "Error - Check log for details";
			this.LastResponse = "Error Response:\n" + result.Error;
		}
		else if (result.Cancelled)
		{
			this.Status = "Cancelled - Check log for details";
			this.LastResponse = "Cancelled Response:\n" + result.RawResult;
		}
		else if (!string.IsNullOrEmpty(result.RawResult))
		{
			this.Status = "Success - Check log for details";
			this.LastResponse = "Success Response:\n" + result.RawResult;

			StartCoroutine(GetUserNameCoroutine(AccessToken.CurrentAccessToken.UserId));
		}
		else
		{
			this.LastResponse = "Empty Response\n";
		}
		Debug.Log (this.LastResponse);
//		LogView.AddLog(result.ToString());
	}

	public void SetScore(double btcValue){
		if (!FB.IsLoggedIn || btcValue < 1)
			return;

		var scoreData = new Dictionary<string, string> ();
		scoreData ["score"] = btcValue.ToString ("F0");

		FB.API ("/me/scores", HttpMethod.POST, delegate (IGraphResult result) {
//			Debug.Log ("Score Submitted successufully" + result.RawResult);
		}, scoreData);
	}

	public void QueryScore(){
		bQueryScore = false;
		FB.API ("/app/scores?fields=score,user.limit(30)", HttpMethod.GET, getScoreCallBack, dic);
//		GetProfileImage ();
	}

	public void getScoreCallBack(IResult result){
		IDictionary<string, object> data = result.ResultDictionary;
		List<object> scoreList = (List<object>)data ["data"];

		LeaderBoardManager.Instance.m_RankingList.Clear ();
		int i = 0;
		foreach (object obj in scoreList) {
			var entry = (Dictionary<string, object>) obj;
			var user = (Dictionary<string, object>) entry["user"];
			Ranking rank = new Ranking ();
			rank.nOrder = i++;
			rank.playerName = user ["name"].ToString ();
			rank.bitcoins = double.Parse(entry["score"].ToString());
			LeaderBoardManager.Instance.m_RankingList.Add (rank);
			if (i > 100)
				break;

//			Debug.Log (user["name"].ToString()+","+entry["score"].ToString());
//			scoreDebug.text = entry ["score"].ToString ();
//
//			GameObject scorePanel;
//			scorePanel = Instantiate (ScoreEntryPanel) as GameObject;
//			scorePanel.transform.SetParent (ScrollSocreList.transform, false);
//
//			Transform FName = scorePanel.transform.Find ("Name-Text");
//			Transform FScore = scorePanel.transform.Find ("Score-Text");
//			Transform FAvatar = scorePanel.transform.Find ("Profile-Image");
//			Text FnameText = FName.GetComponent<Text> ();
//			Text Fscoretext = FScore.GetComponent<Text> ();
//			Image FAvatarImage = FAvatar.GetChild (0).GetComponent<Image> ();
//
//			FnameText.text = user ["name"].ToString ();
//			Fscoretext.text = entry ["score"].ToString ();
//			FB.API (user ["id"].ToString () + "picture?width=120&height=120", HttpMethod.GET, delegate(IGraphResult picResult) {
//				if (picResult.Error != null) {
//					Debug.Log (picResult.RawResult);
//				} else {
//					LeaderBoardManager.Instance.m_ImageAvatarList.Add(Sprite.Create (picResult.Texture, new Rect (0, 0, picResult.Texture.width, picResult.Texture.height), new Vector2 (0.5f, 0.5f)));
//				}
//			});
		}

		bQueryScore = true;
		LeaderBoardManager.Instance.LoadScores ();
	}

	public void ShareScore(){

		ShareMyScore ();
		return;

//		string strScores = GameManager.Instance.m_player.Bitcoins.ToString ("F0") + "BTC, " + 
//			GameManager.Instance.m_player.Blocks.ToString ("F0") + "Blocks, " + GameManager.Instance.m_player.Cashs.ToString ("F0") + "Cashs";
//
//		FB.FeedShare(
//			string.Empty,
//			null,
//			"Share my Score",
//			"Hey Guys! Check out my score:"+strScores,
//			"Check out my score",
//			null,
//			string.Empty,
//			this.HandleResult);
	}

	public void ShareMyScore(){
		if(!ShareImage)
		{			
			StartCoroutine(ShareImageShot());
		}
	}

	IEnumerator ShareImageShot()
	{
		ShareImage = true;

		yield return new WaitForEndOfFrame();
		Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);

		screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height),0,0);

		screenTexture.Apply();

		byte[] dataToSave = screenTexture.EncodeToPNG();

		string destination = Path.Combine(Application.persistentDataPath, Screenshot_Name);

		bQueryScore = false;

		File.WriteAllBytes(destination, dataToSave);

		var wwwForm = new WWWForm();
		wwwForm.AddBinaryData("image", dataToSave, "InteractiveConsole.png");

		FB.API("me/photos", HttpMethod.POST, ShareCallback, wwwForm);

	}

//	private Texture2D lastResponseTexture;
	private string ApiQuery = "";

	void ShareCallback(IResult result)
	{
//		lastResponseTexture = null;
		if (result.Error != null)
			lastResponse = "Error Response:\n" + result.Error;
		else if (!ApiQuery.Contains("/picture"))
			lastResponse = "Success Response:\n" + result.RawResult;
		else
		{
//			lastResponseTexture = result.Texture;
			lastResponse = "Success Response:\n";
		}
		bQueryScore = true;
		Debug.Log ("Share Screenshot:" + lastResponse);
	}
}


