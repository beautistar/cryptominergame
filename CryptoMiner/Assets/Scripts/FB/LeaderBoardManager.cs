﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderBoardManager : MonoBehaviour {
	private static LeaderBoardManager _instance = null;
	public static LeaderBoardManager Instance {get { return _instance;}}
	public List<Ranking> m_RankingList;
	public List<Sprite> m_ImageAvatarList;

	public GameObject rankContent;
	public GameObject rankingItem;
	public GameObject leaderboardButton;
	public Text textBTC, textBlock, textCash;
	public GameObject imageLoading;

	void Awake(){
		_instance = this;	
	}

	// Use this for initialization
	void Start () {
		m_RankingList = new List<Ranking> ();
		m_ImageAvatarList = new List<Sprite> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameManager.Instance.m_bStart)
			return;
		
		if (FBManager.Instance.bLogined && !leaderboardButton.activeSelf) {
			leaderboardButton.SetActive (true);
		} else if (!FBManager.Instance.bLogined && leaderboardButton.activeSelf) {
			leaderboardButton.SetActive (false);
		}

		if (FBManager.Instance.bQueryScore) {
			imageLoading.SetActive (false);
			LoadScores ();
		}
		else
			imageLoading.SetActive (true);
	}

	public void LoadScores(){
		if (FBManager.Instance.bQueryScore) {
			for (int i=rankContent.transform.childCount-1;i>=0;i--) {
				Transform child = rankContent.transform.GetChild (i);
				child.SetParent( null );
				GameObject.Destroy(child.gameObject);
			}

			for (int i=0; i<m_RankingList.Count; i++) {
				GameObject obj = GameObject.Instantiate (Resources.Load("Prefabs/RankingItem")) as GameObject;
				obj.transform.SetParent (rankContent.transform);
				obj.GetComponent<RankingItem> ().TextOrder.text = (m_RankingList [i].nOrder+1).ToString ();
				if (m_RankingList [i].nOrder == 0) {
					Texture2D tex = Resources.Load ("Sprites/UI/1stPlace") as Texture2D;
					Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
					obj.GetComponent<RankingItem> ().ImageOrder.sprite = sprite;
				} else if (m_RankingList [i].nOrder == 1) {
					Texture2D tex = Resources.Load ("Sprites/UI/2ndPlace") as Texture2D;
					Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
					obj.GetComponent<RankingItem> ().ImageOrder.sprite = sprite;
				} else if (m_RankingList [i].nOrder == 2) {
					Texture2D tex = Resources.Load ("Sprites/UI/3rdPlace") as Texture2D;
					Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
					obj.GetComponent<RankingItem> ().ImageOrder.sprite = sprite;
				} else {
					obj.GetComponent<RankingItem> ().ImageOrder.enabled = false;
				}

				obj.GetComponent<RankingItem> ().TextUserName.text = m_RankingList [i].playerName;
				obj.GetComponent<RankingItem> ().TextScore.text = m_RankingList [i].bitcoins.ToString ("F0");
				if (m_ImageAvatarList.Count > i) {
					obj.GetComponent<RankingItem> ().ImageAvatar.sprite = m_ImageAvatarList [i];
					obj.GetComponent<RankingItem> ().ImageAvatar.enabled = true;
				}
				else
					obj.GetComponent<RankingItem> ().ImageAvatar.enabled = false;
				
				obj.transform.localScale = Vector3.one;
				obj.transform.GetComponent<RectTransform> ().localPosition = Vector3.zero;
				obj.name = "RankingItem" + i;
			}
		} else {
			FBManager.Instance.FBLogin ();
		}

		textBTC.text = GameManager.Instance.m_player.Bitcoins.ToString ("F0");
		textBlock.text = GameManager.Instance.m_player.Blocks.ToString ("F0");
		textCash.text = GameManager.Instance.m_player.Cashs.ToString ("F0");
	}

	public void ShareScores(){
		FBManager.Instance.ShareScore ();
	}
}
