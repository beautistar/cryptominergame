﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan : MonoBehaviour {
	public bool bStart = false;
	public bool bRunning = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public virtual void startFan(){
	}

	public virtual void stopFan(){
	}

	public virtual void cancelFan(){
	}
}
