﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallFan : Fan{
	public float m_time;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameManager.Instance.m_bStart)
			return;
		
		if (m_time + 20 < Time.time && bStart)
			stopFan ();
	}

	public override void startFan(){
		if (!bRunning) {
			GetComponent<Animator> ().SetTrigger ("startFan");
			bStart = true;
			GameManager.Instance.setFanFlying (bStart);
			m_time = Time.time;
			GameManager.Instance.Lamp.GetComponent<Animator> ().SetTrigger ("StartLamp");
			GameManager.Instance.startFanAir (true);
			bRunning = true;
			GameManager.Instance.WallFan.GetComponent<Button> ().enabled = false;
			GameManager.Instance.ElectricalEffect.SetActive(true);
			GameManager.Instance.ElectricalEffect.transform.parent.Find ("SwitchOn").GetComponent<Button> ().enabled = false;
		}
	}

	public override void stopFan(){
		if (bRunning) {
			cancelFan ();
		}
	}

	public override void cancelFan(){
		if(bRunning)
			GameManager.Instance.Lamp.GetComponent<Animator> ().SetTrigger ("StopLamp");
		GameManager.Instance.startFanAir (false);
		GetComponent<Animator> ().SetTrigger ("stopFan");
		bStart = false;
		GameManager.Instance.setFanFlying (bStart);
		bRunning = false;
		GameManager.Instance.WallFan.GetComponent<Button> ().enabled = true;
		GameManager.Instance.ElectricalEffect.SetActive(false);
		GameManager.Instance.ElectricalEffect.transform.parent.Find ("SwitchOn").GetComponent<Button> ().enabled = true;
	}
}
