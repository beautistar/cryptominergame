﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JSONParse : MonoBehaviour {
	private static JSONParse _instance;
	public static JSONParse Instance {get {return _instance;}}

	string strFileName="";
	public TextAsset textJson;
	string strJson = "";
	bool bOpened = false;
	ServerAPI serverAPI;

	void Awake(){
		_instance = this;
	}

	void Start(){
		if (getFileName ().Equals (""))
			setFileName ();
		serverAPI = new ServerAPI ();
	}
	void setFileName(){
//		strFileName = "Assets/Resources/Json/GameInfo2.json";
		strFileName = Application.persistentDataPath + "/GameInfo2.json";
	}
	string getFileName (){
		return strFileName;
	}
	public void LoadJSONFile(){		
		if (getFileName ().Equals (""))
			setFileName ();

		if (File.Exists (strFileName) && !bOpened) {
			System.IO.StreamReader reader;
			//reader = new System.IO.StreamReader(Application.persistentDataPath + "/Json/GameInfo.json");
			bOpened = true;
			reader = new System.IO.StreamReader (strFileName);
			string str1 = reader.ReadToEnd ();
			if (!str1.Equals ("") || str1.Length > 100)
				strJson = str1;
			reader.Close ();
			bOpened = false;

			GameManager.Instance.gameInfo = new GameInfo ();
			#if UNITY_ANDROID
			GameManager.Instance.gameInfo = JsonConvert.DeserializeObject<GameInfo>(strJson);
			#elif UNITY_IOS
			GameManager.Instance.gameInfo = JsonUtility.FromJson<GameInfo>(strJson);
			#endif

			GetAPI ();
			//		SaveJSONFile ();
			GameManager.Instance.SetLoaded(true);

		} else {			
			GetJSON ();
		}
//		if(strJson == null)
//			strJson = textJson.text;	

	}

	public void SaveJSONFile(){
		if (bOpened)
			return;
		if (getFileName ().Equals (""))
			setFileName ();

		StreamWriter writer;
		if (File.Exists (strFileName))
			File.Delete (strFileName);
		
		if (GameManager.Instance.gameInfo.UserName == null || GameManager.Instance.gameInfo.positions == null)			
			return;
		
		#if UNITY_ANDROID
		strJson = JsonConvert.SerializeObject (GameManager.Instance.gameInfo);
		#elif UNITY_IOS
		strJson = JsonUtility.ToJson (GameManager.Instance.gameInfo);
		#endif

		if (strJson.Length < 100)
			return;
		
		writer = new StreamWriter (strFileName, true);
		writer.Write (strJson);
		writer.Close ();
	}

	void GetAPI(){
		if (Application.internetReachability == NetworkReachability.NotReachable)
			return;
		
		string url = GameManager.SERVER_API_LINK;
		WWW www = new WWW(url);
		StartCoroutine(WaitForRequestAPI(www));
	}

	IEnumerator WaitForRequestAPI(WWW www)
	{
		yield return www;

		// check for errors
		if (www.error == null)
		{
//			Debug.Log("WWW Ok!: " + www.text);

			#if UNITY_ANDROID
			serverAPI = JsonConvert.DeserializeObject<ServerAPI>(www.text);
			#elif UNITY_IOS
			serverAPI = JsonUtility.FromJson<ServerAPI>(www.text);
			#endif
			if (serverAPI.ticker.price != null || !serverAPI.ticker.price.Equals ("")) {
				GameManager.Instance.gameInfo.BTC2USD = double.Parse (serverAPI.ticker.price);
				GameManager.Instance.m_player.BTC2USD = GameManager.Instance.gameInfo.BTC2USD;
			}

		} else {
			Debug.Log("WWW Error: "+ www.error);
		}    
	}

	void GetJSON(){	
		if (Application.internetReachability != NetworkReachability.NotReachable) {			
			string url = GameManager.SERVER_JSON_LINK;
			WWW www = new WWW (url);
			StartCoroutine (WaitForRequestJSON (www));
		} else {
			LoadLocalJson ();
		}
	}

	IEnumerator WaitForRequestJSON(WWW www)
	{
		yield return www;

		// check for errors
		if (www.error == null && !www.text.Contains("Error"))
		{			
			//			Debug.Log("WWW Ok!: " + www.text);
			strJson = www.text;

			GameManager.Instance.gameInfo = new GameInfo ();
			#if UNITY_ANDROID
			GameManager.Instance.gameInfo = JsonConvert.DeserializeObject<GameInfo>(strJson);
			#elif UNITY_IOS
			GameManager.Instance.gameInfo = JsonUtility.FromJson<GameInfo>(strJson);
			#endif

			GetAPI ();
			//		SaveJSONFile ();
			GameManager.Instance.SetLoaded(true);

		} else {
			LoadLocalJson ();
		}    
	}

	void LoadLocalJson(){
		strJson = textJson.text;	
		GameManager.Instance.gameInfo = new GameInfo ();
		#if UNITY_ANDROID
		GameManager.Instance.gameInfo = JsonConvert.DeserializeObject<GameInfo>(strJson);
		#elif UNITY_IOS
		GameManager.Instance.gameInfo = JsonUtility.FromJson<GameInfo>(strJson);
		#endif

		GameManager.Instance.SetLoaded(true);
	}
}

