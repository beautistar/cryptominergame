﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExchangeScreen : MonoBehaviour {
	private static ExchangeScreen _instance = null;
	public static ExchangeScreen Instance {get { return _instance;}}

	public Text textBTC, textCash, textRate;
	public Slider slider;
	public Button btnExchange;

	double btcVal = 0;
	double cashVal = 0;
	double diffBTC = 0;

	void Awake(){
		_instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable(){
		btcVal = GameManager.Instance.m_player.Bitcoins;
		cashVal = 0;
		textBTC.text = btcVal.ToString ("F10");
		textCash.text = "$ 0";
		textRate.text = "$ " + GameManager.Instance.m_player.BTC2USD.ToString ("F2");
		SetExchangeButton ();
	}

	public void OnExchangeSlider(){
		diffBTC = btcVal - btcVal * slider.value;
		cashVal = btcVal * slider.value * GameManager.Instance.m_player.BTC2USD;
		textBTC.text = diffBTC.ToString ("F10");
		textCash.text = "$ " + cashVal.ToString ("F0");
		SetExchangeButton ();
	}

	void SetExchangeButton(){
		if (cashVal > 0) {
			btnExchange.interactable = true;
		} else {
			btnExchange.interactable = false;
		}
	}

	public void OnExchangeButton(){
		if (cashVal > 0) {
			GameManager.Instance.m_player.Bitcoins = diffBTC;
			GameManager.Instance.m_player.Cashs += cashVal;
			GameManager.Instance.UpdateUIValues ();

			slider.value = 0;
		}
	}
}
