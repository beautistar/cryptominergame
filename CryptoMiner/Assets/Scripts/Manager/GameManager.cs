﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	private static GameManager _instance = null;
	public static GameManager Instance {get { return _instance;}}

	public Player m_player;
	public GameObject SplashScreen;
	public GameObject MainScreen;
	public GameObject MinerSelectScreen;
	public GameObject IncomeScreen;
	public GameObject ChooseUserNameScreen;
	public GameObject WhileGoneScreen;
	public GameObject SettingScreen;

	public GameObject[] m_Positions;
	public GameObject WallFan;
	public GameObject Lamp, FanAir;
	public GameObject ElectricalEffect;
	public GameObject PCCode, codeLeft, codeRight;
	public GameObject PCTimer;
	public Text txtInputUserName;
	public Text txtRankUserName;
	public Text txtWhileGoneValue;

	public int m_selPosition = 0;

	public Text m_txtBitcoins, m_txtBlocks, m_txtCashs, m_txtBTCPMin;
	public bool m_FanFlying = false;
	public bool m_PCRunning = false;
	public bool m_bStart =  false;

	public GameInfo gameInfo;

	public static string SERVER_API_LINK = "https://api.cryptonator.com/api/ticker/btc-usd";
	public static string SERVER_JSON_LINK = "http://www.baocorp.net/Games/CryptoMiner/GameInfo.json";

	float m_Time = 0;
	float m_PCTime = 0;
	float m_minute = 0;
	DateTime m_PCTimer;
	private float t;                    // Timer seconds ticker
	double oldBTC = 0;

	bool bLoaded = false, bInit = false;

	void Awake(){
		_instance = this;
	}

	// Use this for initialization
	void Start () {	
		gameInfo = new GameInfo ();
		JSONParse.Instance.LoadJSONFile ();
	}
	
	// Update is called once per frame
	void Update () {
		if (bLoaded && !bInit) {
			Init ();
			bLoaded = false;
			bInit = true;
		}
			
		if (m_bStart) {
			checkAvailableMiner ();
			checkPCRunning ();
			updateBTCPMin ();
			UpdateUIValues ();
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			JSONParse.Instance.SaveJSONFile ();
			Application.Quit ();
		}
	}

	void Init(){
		m_player = new Player ();
		m_player.Init ();

		//Init IAPController
		new IAPController ();

		SplashScreen.SetActive (true);
		MainScreen.SetActive (false);
		FanAir.SetActive (false);
		showMinerSelectScreen (false);
		showPCTimer (false);
		showIncomeScreen (false);
		showSettingScreen (false);

		Invoke ("showMainScreen", 5.0f);
		setTime ();

		if (m_player.Name.Equals ("") || m_player.Name == null)
			showChooseNameScreen (true);
		else {
			showChooseNameScreen (false);
			m_bStart = true;
		}

		checkWhileGoneScreen ();

		m_txtBitcoins.text = m_player.Bitcoins.ToString ("F10");
		m_txtBlocks.text = m_player.Blocks.ToString ();
		m_txtCashs.text = m_player.Cashs.ToString ("F0");
		m_txtBTCPMin.text = m_player.BTCPMin.ToString ("F10");

		oldBTC = gameInfo.Bitcoins;
		if (m_player.Miners == 0 && m_player.Blocks == 0 && m_player.Cashs == 3000)
			oldBTC = 0;

		if (gameInfo.Miners > 0) {
			for (int i = 0; i < m_Positions.Length; i++) {
			
				if (gameInfo.positions [i].minerNumber > 0) {
					m_Positions [i].GetComponent<Position> ().bBaught = true;
					m_Positions [i].GetComponent<Position> ().putMiner (gameInfo.positions [i].minerNumber-1);
				}
			}
		}

		//Reset ();
	}

	void Reset(){
		m_player.Reset ();
		for (int i = 0; i < m_Positions.Length; i++) {
			gameInfo.positions [i].minerNumber = 0;
			m_Positions [i].GetComponent<Position> ().bBaught = false;
		}
	}
		
	void OnDestroy(){
		if (m_player != null){
			m_player.m_Time = DateTime.Now;
			m_player.SavePlayingTime ();
		}
		JSONParse.Instance.SaveJSONFile ();
	}

	void OnApplicationPause(bool bPause){
		if (bPause) {
			JSONParse.Instance.SaveJSONFile ();
		} else {
			JSONParse.Instance.LoadJSONFile ();
		}
	}

	void showMainScreen(){
		SplashScreen.SetActive (false);
		MainScreen.SetActive (true);
	}

	public void setTextBitcoins(string val){
		m_txtBitcoins.text = val;
//		FBManager.Instance.SetScore (m_player.Bitcoins);
	}

	public void setTextCashCoins(string val){
		m_txtCashs.text = "$ " + val;
	}

	public void setTextBlocks(string val){
		m_txtBlocks.text = val;
	}

	public void setTextBTCPMin(string val){
		m_txtBTCPMin.text = val;
	}

	void checkAvailableMiner(){
		if (m_player == null)
			return;
		
		m_player.Miners = 0;

		for (int i = 0; i < m_Positions.Length; i++) {
			Position position = m_Positions [i].GetComponent<Position> ();
			if (m_player.Cashs >= position.m_Cost && !position.bBaught) {
				position.m_buy.SetActive(true);					
			}
			else if (m_player.Cashs < position.m_Cost && !position.bBaught) {
				position.m_buy.SetActive(false);					
			}
			if (position.bBaught)
				m_player.Miners++;
		}

//		m_txtFounds.text = m_player.Miners.ToString ();
	}

	public void setTime(){
		m_Time = Time.time;
	}

	void setMinute(){
		m_minute = Time.time;
	}

	public void checkGetBlock(){
		if (m_Time + gameInfo.BlockSeconds < Time.time) {
			m_player.Blocks++;
//			m_txtBlocks.text = m_player.Blocks.ToString ();
			setTime ();
		}
		
	}

	public void startFanAir (bool bFlag){		
		FanAir.SetActive (bFlag);
		if(bFlag)
			FanAir.GetComponent<Animator> ().SetTrigger ("StartFanAir");
		else
			FanAir.GetComponent<Animator> ().SetTrigger ("StopFanAir");
	}

	public bool getFanFlying(){
		return m_FanFlying;
	}

	public void setFanFlying(bool bFlag){
		m_FanFlying = bFlag;
	}

	public bool getPCRunning(){
		return m_PCRunning;
	}

	public void setPCRunning(bool bFlag){
		m_PCRunning = bFlag;
		if (bFlag) {
			m_PCTime = Time.time;
			PCCode.GetComponent<Animator> ().SetTrigger ("PCStart");
			codeLeft.SetActive (true);
			codeLeft.GetComponent<Animator> ().SetTrigger ("codeLeft");
			codeRight.SetActive (true);
			codeRight.GetComponent<Animator> ().SetTrigger ("codeRight");
			codeLeft.transform.parent.GetComponent<Button> ().enabled = false;
			codeRight.transform.parent.GetComponent<Button> ().enabled = false;
			PCCode.transform.parent.GetComponent<AudioSource> ().Play ();
		} else {
			codeLeft.SetActive (false);
			codeRight.SetActive (false);
		}
	}

	void checkPCRunning(){
		if (PCTimer.activeSelf) {
			t += Time.deltaTime;
			if (t >= 1) {
				m_PCTimer = m_PCTimer.AddSeconds (-1);
				t = 0;
			}

			string formattedTs = string.Format ("{0:D2}:{1:D2}:{2:D2}", m_PCTimer.Hour, m_PCTimer.Minute, m_PCTimer.Second);

			PCTimer.transform.Find("TextTimer").GetComponent<Text>().text = formattedTs;
		}

		if (getPCRunning() && (m_PCTime + gameInfo.PCSeconds < Time.time)) {
//		if (getPCRunning() && (m_PCTime + 10) < Time.time) {
			setPCRunning (false);
			codeLeft.GetComponent<Animator> ().SetTrigger ("PCStop");
			codeRight.GetComponent<Animator> ().SetTrigger ("PCStop");
			codeLeft.transform.parent.GetComponent<Button> ().enabled = true;
			codeRight.transform.parent.GetComponent<Button> ().enabled = true;
			PCCode.transform.parent.GetComponent<AudioSource> ().Stop ();
			showPCTimer (false);
		}
	}

	public void cancelPCRunning(){
		setPCRunning (false);
		codeLeft.GetComponent<Animator> ().SetTrigger ("PCStop");
		codeRight.GetComponent<Animator> ().SetTrigger ("PCStop");
		codeLeft.transform.parent.GetComponent<Button> ().enabled = true;
		codeRight.transform.parent.GetComponent<Button> ().enabled = true;
		PCCode.transform.parent.GetComponent<AudioSource> ().Stop ();
		showPCTimer (false);
	}

	public void showElectricEffect(){
		ElectricalEffect.SetActive (true);
		Invoke ("hideElectricEffect", 1.253f);
	}

	void hideElectricEffect(){
		ElectricalEffect.SetActive (false);
		ElectricalEffect.transform.parent.transform.Find("SwitchOn").GetComponent<Button> ().enabled = true;
	}

	public void showMinerSelectScreen(bool bShow){
		MinerSelectScreen.SetActive (bShow);
	}

	public void setSelectedPosition(int position){
		m_selPosition = position;				
	}

	public void buyMiner(int nNumber){
		m_Positions [m_selPosition - 1].GetComponent<Position> ().BuyMiner (nNumber);
	}

	public void ShowAdsActive(string actionName){
		VungleAdsManager.Instance.DisplayAds (actionName);
	}

	void updateBTCPMin(){
		if (m_minute + 60 < Time.time) {
			int mins = (int)((m_minute + 60.0f) / 60.0f);
			double fVal = (m_player.Bitcoins - oldBTC)/ mins;
//			m_txtBTCPMin.text = fVal.ToString ("F10");
			if (fVal < 0)
				fVal = 0;
			m_player.BTCPMin = fVal;

			setMinute ();
		}
	}

	public void UpdateUIValues(){
		setTextBitcoins (m_player.Bitcoins.ToString ("F10"));
		setTextCashCoins (m_player.Cashs.ToString ("F0"));
		setTextBlocks (m_player.Blocks.ToString ());
		setTextBTCPMin (m_player.BTCPMin.ToString ("F10"));
		txtRankUserName.text = m_player.Name;
	}

	public void SetRankUserName(string strName){
//		m_player.Name = strName;
		txtRankUserName.text = strName;
	}

	public void showPCTimer(bool bFlag){
		PCTimer.SetActive (bFlag);
		if (bFlag) {
			DateTime time = new DateTime ();
			time = time.AddSeconds (gameInfo.PCSeconds);
			m_PCTimer = m_PCTimer.Add (new TimeSpan (0, time.Hour, time.Minute, time.Second));
		}
	}

	public void showIncomeScreen(bool bShow){
		IncomeScreen.SetActive (bShow);
	}

	public void showChooseNameScreen(bool bShow){
		ChooseUserNameScreen.SetActive (bShow);
	}

	public void startMining(){
		if (!txtInputUserName.text.Equals ("")) {
			m_player.Name = txtInputUserName.text;
			gameInfo.UserName = m_player.Name;
			m_bStart = true;
			showChooseNameScreen (false);
		}
	}

	public void showWhileGoneScreen(bool bShow){
		WhileGoneScreen.SetActive(bShow);
	}

	public void checkWhileGoneScreen (){
		if (!m_player.GetPlayingTime ().Equals (DateTime.MinValue) ) {
			TimeSpan t = DateTime.Now - m_player.GetPlayingTime ();
			double fVal = t.TotalMinutes * gameInfo.BTCPMins;
			if (fVal > 0) {
				showWhileGoneScreen (true);
				txtWhileGoneValue.text = fVal.ToString ("F10");

				UpdateUIValues ();
			} else {
				showWhileGoneScreen (false);
			}
		} else {
			showWhileGoneScreen (false);
		}
	}

	public void updateWhileGoneValue(){
		double fVal = double.Parse (txtWhileGoneValue.text);
		m_player.Bitcoins += fVal;
//		m_player.Cashs += fVal * m_player.BTC2USD;
	}

	public void showSettingScreen(bool bShow){
		SettingScreen.SetActive (bShow);
	}

	public void BuyConsumable(int id){
		IAPController.instance.BuyConsumable (id, () => {

			switch (id){
			case 1:
				m_player.Blocks += 10;
				break;
			case 0:
				m_player.Bitcoins += 0.5;
				break;
			}
		});
	}

	public void SetLoaded(bool bFlag){
		if(!bLoaded)
			bLoaded = bFlag;
	}
}
