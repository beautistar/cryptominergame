﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncomeScreen : MonoBehaviour {
	public Text textPlaceholder;
	public Text textName;
	public Text txtSec, txtMin, txtHour, txtDay, txtWeek, txtMonth, txtYear;

	void Awake(){
	}
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void showInfomation(){
		textName.text = GameManager.Instance.gameInfo.UserName;
		textPlaceholder.text = GameManager.Instance.gameInfo.UserName;

		double btcPMin = GameManager.Instance.m_player.BTCPMin;
		txtSec.text = (btcPMin / 60.0f).ToString ("F10");
		txtMin.text = (btcPMin).ToString ("F10");
//		txtHour.text = (btcPMin * 60.0f).ToString ("F10");
		txtDay.text = (btcPMin * 3600.0f * 24.0f).ToString ("F10");
		txtWeek.text = (btcPMin * 3600.0f * 24.0f * 7.0f).ToString ("F10");
		txtMonth.text = (btcPMin * 3600.0f * 24.0f * 30.0f).ToString ("F10");
		txtYear.text = (btcPMin * 3600.0f * 24.0f * 365.0f).ToString ("F10");
	}

	public void changeWorkerName(){
		string strName = textName.text;
		if (!strName.Equals ("")) {
			GameManager.Instance.gameInfo.UserName = textName.text;
			GameManager.Instance.m_player.Name = textName.text;
			GameManager.Instance.UpdateUIValues ();
		}
	}
}
