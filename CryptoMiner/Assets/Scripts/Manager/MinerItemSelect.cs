﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinerItemSelect : MonoBehaviour {
	public GameObject[] minerItems;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable(){
		showMinerItems (false);
		for (int i = 0; i < minerItems.Length; i++) {
			if (GameManager.Instance != null) {
				double cost = GameManager.Instance.gameInfo.miners [i].Cost;
				if (GameManager.Instance.m_player.Cashs >= cost) {
					showMinerItem (i, true);
				} else {
					showMinerItem (i, false);
				}
			}
		}
	}

	void showMinerItems(bool bShow){
		for (int i = 0; i < minerItems.Length; i++) {
			showMinerItem (i, false);
		}
	}

	void showMinerItem(int num, bool bShow){
		Text textCost = minerItems [num].transform.Find ("TextCost").GetComponent<Text> () as Text;
		Text textPay = minerItems [num].transform.Find ("TextPay").GetComponent<Text> () as Text;
		if (GameManager.Instance != null) {
			textCost.text = "Cost:  $" + GameManager.Instance.gameInfo.miners [num].Cost.ToString("F0");
			textPay.text = "Pay:" + GameManager.Instance.gameInfo.miners [num].PayPerSecond.ToString("F10");
		}
		if(bShow)
			minerItems [num].GetComponent<Image> ().color = new Color32 (255, 255, 255, 255); 
		else
			minerItems [num].GetComponent<Image> ().color = new Color32 (100, 100, 100, 100); 
		minerItems [num].GetComponent<Button> ().enabled = bShow;
	}
}
