﻿using System.Collections;
using System;
using System.Globalization;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardManager : MonoBehaviour {
	private static RewardManager _instance = null;
	public static RewardManager Instance {get { return _instance;}}
	public GameObject RewardsParent;
	public GameObject[] RewardItems;
	// Claim Button
	public Button btnClaim;

	// How long until next claim
	public Text txtTimeDue;

	public DateTime timer;              // Today timer
	public DateTime lastRewardTime;     // The last time the user clicked in a reward

	[HideInInspector]
	public int availableReward;         // The available reward position the user can click

	[HideInInspector]
	public int lastReward;              // the last reward the user clicked

	private float t;                    // Timer seconds ticker
	private bool isInitialized;         // Is the timer initialized?

	// Needed Constants
	private const string LAST_REWARD_TIME = "LastRewardTime";
	private const string LAST_REWARD = "LastReward";
	private const string FMT = "O";
	bool bReadyClaim = false;

	void Awake(){
		_instance = this;	

	}

	// Use this for initialization
	void Start () {
//		DontDestroyOnLoad (gameObject);	
		RewardItems = new GameObject[30];
		CheckRewards ();
		//UpdateUI ();
	}

	// Update is called once per frame
	void Update () {
		if (!GameManager.Instance.m_bStart)
			return;
		
		t += Time.deltaTime;
		if (t >= 1)
		{
			timer = timer.AddSeconds(1);
			t = 0;
		}

		if (!bReadyClaim) {
			TimeSpan difference = (lastRewardTime - timer).Add (new TimeSpan (0, 24, 0, 0));

			// Is the counter below 0? There is a new reward then
			if (difference.TotalSeconds <= 0) {
				CheckRewards ();
				UpdateUI ();
				return;
			}

			string formattedTs = string.Format ("{0:D2}h{1:D2}m{2:D2}s", difference.Hours, difference.Minutes, difference.Seconds);

			txtTimeDue.text = "New reward in " + formattedTs;
		} else {
			txtTimeDue.text = "Ready to Claim!";
		}
	}

	void OnEnable(){
		CheckRewards();
		UpdateUI();
	}

	private void Initialize()
	{
		timer = DateTime.Now;
		isInitialized = true;
	}

	public void LoadRewardUI(){
		for (int i = 0; i < RewardsParent.transform.childCount; i++) {
			Transform child = RewardsParent.transform.GetChild(i);
			RewardItems [i] = child.gameObject;

			string str = GameManager.Instance.gameInfo.Reward [i].nValue.ToString ();
			switch (GameManager.Instance.gameInfo.Reward [i].rewardType) {
			case RewardType.hour:
				str = "x"+GameManager.Instance.gameInfo.Reward [i].nValue.ToString ()+"H";
				break;
			case RewardType.times:
				str = "x"+GameManager.Instance.gameInfo.Reward [i].nValue.ToString ();
				break;
			case RewardType.percent:
				str = GameManager.Instance.gameInfo.Reward [i].nValue.ToString () + "%";
				break;
			}
			child.GetComponent<RewardItem> ().setTextValue (str);

			string strIconName = "iconBlock";
			switch (GameManager.Instance.gameInfo.Reward [i].valueType) {
			case RewardValueType.Cash:
				strIconName = "iconCash";
				break;
			case RewardValueType.BTCPMin:
				strIconName = "iconBTCPMin";
				break;
			case RewardValueType.BTC:
				strIconName = "iconBTC";
				break;
			case RewardValueType.Block:				
			default:
				strIconName = "iconBlock";
				break;
			}
				
			Texture2D tex = Resources.Load ("Sprites/UI/" + strIconName) as Texture2D;
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			child.GetComponent<RewardItem> ().setImageIcon (sprite);
		}
	}

	// Check if the player have unclaimed prizes
	public void CheckRewards()
	{
		if (!isInitialized)
		{
			Initialize();
		}

		string lastClaimedTimeStr = PlayerPrefs.GetString(LAST_REWARD_TIME);
		lastReward = PlayerPrefs.GetInt(LAST_REWARD);

		// It is not the first time the user claimed.
		// I need to know if he can claim another reward or not
		if (!string.IsNullOrEmpty(lastClaimedTimeStr))
		{
			lastRewardTime = DateTime.ParseExact(lastClaimedTimeStr, FMT, CultureInfo.InvariantCulture);

			TimeSpan diff = timer - lastRewardTime;
//			Debug.Log("Last claim was " + (long)diff.TotalHours + " hours ago.");

			int days = (int)(Math.Abs(diff.TotalHours) / 24);

			if (days == 0)
			{
				// No claim for you. Try tomorrow
				availableReward = 0;
				return;
			}

			// The player can only claim if he logs between the following day and the next.
			if (days >= 1 && days < 2)
			{
				// If reached the last reward, resets to the first restarting the cicle
				if (lastReward == RewardItems.Length)
				{
					OnResetClick ();
					availableReward = 1;
					lastReward = 0;
					return;
				}
				availableReward = lastReward + 1;

//				Debug.Log("Player can claim prize " + availableReward);
				return;
			}

			if (days >= 2)
			{
				// The player loses the following day reward and resets the prize
				availableReward = 1;
				lastReward = 0;
//				Debug.Log("Prize reset ");
			}
		}
		else
		{
			// Is this the first time? Shows only the first reward
			availableReward = 1;
		}
	}

	// Claims the prize and checks if the player can do it
	public void ClaimPrize(int day)
	{
		if (availableReward == day)
		{
			OnClaimPrize(day);

//			Debug.Log("Reward [" + RewardItems[day - 1].name + "] Claimed!");
			PlayerPrefs.SetInt(LAST_REWARD, availableReward);

			string lastClaimedStr = timer.ToString(FMT);
			PlayerPrefs.SetString(LAST_REWARD_TIME, lastClaimedStr);
		}
		else if (day <= lastReward)
		{
			OnPrizeAlreadyClaimed(day);

//			Debug.Log("Reward already claimed. Try again tomorrow");
		}
		else
		{
//			Debug.Log("Cannot Claim this reward! Can only claim reward #" + availableReward);
		}

		CheckRewards();
	}

	public void Reset()
	{
		PlayerPrefs.DeleteKey(RewardManager.LAST_REWARD);
		PlayerPrefs.DeleteKey(RewardManager.LAST_REWARD_TIME);
	}

	// Clicked the claim button
	public void OnClaimClick()
	{
		ClaimPrize(availableReward);
		UpdateUI();
	}

	public void UpdateUI()
	{
		bool isRewardAvailableNow = false;
		if (RewardItems.Length == RewardsParent.transform.childCount) {
			for (int i = 0; i < RewardsParent.transform.childCount; i++) {
//			int reward = DailyRewards.instance.rewards[i];
				int reward = 100;
				int day = i + 1;

				Transform child = RewardsParent.transform.GetChild (i);

				RewardItems [i] = child.gameObject;
				RewardItem rewardItem = RewardItems [i].GetComponent<RewardItem> ();

				rewardItem.day = day;
				rewardItem.reward = reward;

				rewardItem.isAvailable = day == availableReward;
				rewardItem.isClaimed = day <= lastReward;

				if (rewardItem.isAvailable) {
					isRewardAvailableNow = true;
				}

				rewardItem.Refresh ();
			}
		}

		bReadyClaim = isRewardAvailableNow;
		btnClaim.interactable = isRewardAvailableNow;
//		txtTimeDue.gameObject.SetActive(!isRewardAvailableNow);
	}

	// Delegate
	private void OnClaimPrize(int day)
	{
		RewardItems [day - 1].GetComponent<RewardItem> ().ProcessReward ();
//		panelReward.SetActive(true);
//		txtReward.text = "You got " + DailyRewards.instance.rewards[day-1] + " coins!";
	}

	// Delegate
	private void OnPrizeAlreadyClaimed(int day)
	{
		// Do Something with the prize already claimed
	}

	// Close the Rewards panel
	public void OnCloseRewardsClick()
	{
//		panelReward.SetActive(false);
	}

	// Resets player preferences
	public void OnResetClick()
	{
		Reset();
		lastRewardTime = System.DateTime.MinValue;
		CheckRewards();
		for (int i = 0; i < RewardsParent.transform.childCount; i++) {
			RewardItem rewardItem = RewardItems [i].GetComponent<RewardItem> ();
			rewardItem.InitSelect ();
		}
		UpdateUI();
	}

	// Simulates the next day
	public void OnAdvanceDayClick()
	{
		timer = timer.AddDays(1);
		CheckRewards();
		UpdateUI();
	}

	// Simulates the next hour
	public void OnAdvanceHourClick()
	{
		timer = timer.AddHours(1);
		CheckRewards();
		UpdateUI();
	}
}
