﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingManager : MonoBehaviour {
	public GameObject btnSound, btnMusic, btnHelp, btnNoti, btnRate, btnSync;

	public AudioSource[] audioSources;
	public AudioSource audioMusic;

	public bool m_bSound = true;
	public bool m_bMusic = true;
	public bool m_bNotification = false;

	// Use this for initialization
	void Start () {
		Init ();
		UpdateMusicButtonState ();
		UpdateSoundButtonState ();
		UpdateNotiButtonState ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Init(){
		Texture2D tex = Resources.Load("Sprites/UI/"+"btnSoundOff") as Texture2D;
		if (tex != null) {
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			btnSound.GetComponent<Image> ().sprite = sprite;
			btnSound.GetComponent<Image> ().sprite.name = sprite.name;
		}

		tex = Resources.Load("Sprites/UI/"+"btnMusicOff") as Texture2D;
		if (tex != null) {
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			btnMusic.GetComponent<Image> ().sprite = sprite;
			btnMusic.GetComponent<Image> ().sprite.name = sprite.name;
		}

		tex = Resources.Load("Sprites/UI/"+"btnNotificationOff") as Texture2D;
		if (tex != null) {
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			btnNoti.GetComponent<Image> ().sprite = sprite;
			btnNoti.GetComponent<Image> ().sprite.name = sprite.name;
		}			
	}

	public void EnalbeSound(bool bEnable){
		for(int i=0; i<audioSources.Length; i++){
			audioSources [i].enabled = bEnable;
//			if(bEnable && audioSources [i].enabled)
//				audioSources [i].Play ();
//			else
//				audioSources [i].Pause ();
		}
	}

	public void EnableMusic(bool bEnable){
		if (bEnable)
			audioMusic.Play ();
		else
			audioMusic.Pause ();
	}

	public void UpdateSoundButtonState(){
		Texture2D tex = Resources.Load ("Sprites/UI/" + "btnSoundOff") as Texture2D;
		string strName = "";
		if (m_bSound) {
			strName = "btnSoundOn";
			EnalbeSound (true);
		} else {
			strName = "btnSoundOff";
			EnalbeSound (false);
		}
		tex = Resources.Load ("Sprites/UI/" + strName) as Texture2D;
		if (tex != null) {
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			btnSound.GetComponent<Image> ().sprite = sprite;
			btnSound.GetComponent<Image> ().sprite.name = sprite.name;
		}
	}
	public void UpdateMusicButtonState(){
		Texture2D tex = Resources.Load ("Sprites/UI/" + "btnMusicOn") as Texture2D;
		string strName = "";
		if (m_bMusic) {
			strName = "btnMusicOn";
			EnableMusic (true);
		} else {
			strName = "btnMusicOff";
			EnableMusic (false);
		}
		tex = Resources.Load ("Sprites/UI/" + strName) as Texture2D;
		if (tex != null) {
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			btnMusic.GetComponent<Image> ().sprite = sprite;
			btnMusic.GetComponent<Image> ().sprite.name = sprite.name;
		}
	}

	public void UpdateNotiButtonState(){
		Texture2D tex = Resources.Load ("Sprites/UI/" + "btnNotificationOff") as Texture2D;
		string strName = "";
		if (m_bNotification) {
			strName = "btnNotificationOn";
		} else {
			strName = "btnNotificationOff";
		}
		tex = Resources.Load("Sprites/UI/"+strName) as Texture2D;
		if (tex != null) {
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			btnNoti.GetComponent<Image> ().sprite = sprite;
			btnNoti.GetComponent<Image> ().sprite.name = sprite.name;
		}
	}

	public void SetMusic(){
		m_bMusic = !m_bMusic;
		UpdateMusicButtonState ();
	}

	public void SetSound(){
		m_bSound = !m_bSound;
		UpdateSoundButtonState ();
	}

	public void SetNotification(){
		m_bNotification = !m_bNotification;
		UpdateNotiButtonState ();
	}
}
