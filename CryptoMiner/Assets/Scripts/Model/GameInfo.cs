﻿using System;

[System.Serializable]
public class GameInfo
{
	public string UserName ;
	public double Coins ;
	public double Bitcoins ;
	public int Blocks ;
	public double Cashs;
	public int Miners ;
	public double BTCPMins;
	public double EarnedCost ;
	public int ProgressTime ;
	public int PCSeconds ;
	public int BlockSeconds ;
	public double BTC2USD ;
	public string PlayingTime ;
	public Positions[] positions ;
	public Miners[] miners ;
	public PayWithBlock[] paywithblock ;
	public PayWithCash[] paywithcash;
	public PayWithBTC[] paywithbtc ;
	public Reward[] Reward ;

	public GameInfo ()
	{
	}
}
[System.Serializable]
public class Positions
{
	public int id ;
	public int minerNumber ;
	public double Cost ;
}
[System.Serializable]
public class Miners
{
	public int id ;
	public string name ;
	public double Cost ;
	public double PayPerSecond ;	
}
[System.Serializable]
public class PayWithBlock
{
	public int id ;
	public string name ;
	public int hours ;
	public int blocks ;
	public string desc ;	
	public int paid;
}
[System.Serializable]
public class PayWithCash
{
	public int id ;
	public string name ;
	public string content ;
	public double btc ;
	public int cost ;
	public string desc ;
	public int paid;
}
[System.Serializable]
public class PayWithBTC
{
	public int id ;
	public string name ;
	public string content ;
	public BTCUpgradeType type ;
	public double value ;
	public string detail ;
	public double btc ;
	public string desc ;
	public int paid;
}

public enum RewardType{
	plus = 0,
	hour,
	percent,
	times
}

public enum RewardValueType{
	Block,
	Cash,
	BTCPMin,
	BTC
}
[System.Serializable]
public class Reward
{
	public int id ;
	public RewardType rewardType ;
	public RewardValueType valueType ;
	public int nValue ;
}


[System.Serializable]
public class ServerAPI
{
	public Ticker ticker;
	public string timestamp;
	public string success;
	public string error;
}
[System.Serializable]
public class Ticker
{
	//	public string base;
	public string target;
	public string price;
	public string volume;
	public string change;
}
