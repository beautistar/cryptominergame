﻿using System;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
using UnityEngine;

public class Player{
	private string m_Name = "Player";
	private double m_Bitcoins = 0f;
	private int m_Blocks = 0;
	private double m_Cashs = 0;
	private int m_Miners = 0;
	private double m_BTCPMin = 0.0f;
	public double BTC2USD = 3950.01f;
	public DateTime m_Time;
	private string m_playingtimeStr;

	private const string FMT = "O";

	public void Init(){
		//Reset ();

		Name = GameManager.Instance.gameInfo.UserName;
		Bitcoins = GameManager.Instance.gameInfo.Bitcoins;
		Blocks = GameManager.Instance.gameInfo.Blocks;
		Cashs = GameManager.Instance.gameInfo.Cashs;
		Miners = GameManager.Instance.gameInfo.Miners;
		BTC2USD = GameManager.Instance.gameInfo.BTC2USD;
//		m_Time = DateTime.MinValue;
//		m_Time = DateTime.Now;
	}

	public DateTime GetPlayingTime(){
//		m_playingtimeStr = PlayerPrefs.GetString ("PlayingTime", DateTime.MinValue.ToString());
		m_playingtimeStr = GameManager.Instance.gameInfo.PlayingTime;
		if(m_playingtimeStr.Equals(""))
			m_playingtimeStr = DateTime.MinValue.ToString(FMT);
		m_Time = PlayingTime;
		return m_Time;
	}

	public void SavePlayingTime(){
		string str = m_Time.ToString (FMT);
//		Debug.Log (str);
		GameManager.Instance.gameInfo.PlayingTime = str;
//		PlayerPrefs.SetString ("PlayingTime", str);
	}

	public void Reset(){
		GameManager.Instance.gameInfo.UserName = "Jamse";
		GameManager.Instance.gameInfo.Bitcoins = 0;;
		GameManager.Instance.gameInfo.Blocks = 0;
		GameManager.Instance.gameInfo.Cashs = 0;
		GameManager.Instance.gameInfo.Miners = 0;
		GameManager.Instance.gameInfo.BTC2USD = 3950.01f;
	}

	public string Name{
		get{ return m_Name; }
		set{ 
			m_Name = value; 
			GameManager.Instance.gameInfo.UserName = m_Name;
		}
	}

	public double Bitcoins{
		get{ return m_Bitcoins; }
		set{
			m_Bitcoins = value; 
			GameManager.Instance.gameInfo.Bitcoins = m_Bitcoins;
		}
	}
	public int Blocks{
		get{ return m_Blocks; }
		set{
			m_Blocks = value; 
			GameManager.Instance.gameInfo.Blocks = m_Blocks;
		}
	}
	public double Cashs{
		get{ return m_Cashs; }
		set{
			m_Cashs = value; 
			GameManager.Instance.gameInfo.Cashs = m_Cashs;
		}
	}
	public int Miners{
		get{ return m_Miners; }
		set{
			m_Miners = value; 
			GameManager.Instance.gameInfo.Miners = m_Miners;
		}
	}
	public double BTCPMin{
		get{ return m_BTCPMin; }
		set{
			m_BTCPMin = value; 
			GameManager.Instance.gameInfo.BTCPMins = m_BTCPMin;
		}
	}

	public DateTime PlayingTime{
		get{
			DateTime t = DateTime.ParseExact(m_playingtimeStr, FMT, CultureInfo.InvariantCulture);
//			Debug.Log (t);
			return t; }
		set{
			m_Time = value; 
		}
	}

}
