﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Position : MonoBehaviour {
	private static Position _instance = null;
	public static Position Instance {get{ return _instance;}}

	public GameObject m_buy;
	public GameObject m_image;
	public Text m_textValue;
	public GameObject progressBar;
	public GameObject SellFor;
	public GameObject TapImage;

	public bool bBaught = false;
	public int m_Number = 0;

	public int m_MinerNumber = 0;
	public double m_Cost = 0;
	public double m_PayperSec = 0;
	public double m_ProgressTime = 5.0f;
	public double m_EarnedBTC = 0;
	public GameObject[] m_Fans;

//	bool bLoading = false;
	float m_time = 0;
	//float m_barTime = 0;

	// Use this for initialization
	void Start () {
		//Reset ();
		SellFor.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameManager.Instance.m_bStart)
			return;
		
		if (m_time + GameManager.Instance.gameInfo.ProgressTime < Time.time) {
//			bLoading = false;
//			progressBar.GetComponent<Animator> ().SetTrigger ("startMiner");

			if (bBaught) {
				m_time = Time.time;
				double fRate = (GameManager.Instance.getFanFlying () ? 2.0f : 1);
				fRate *= (GameManager.Instance.getPCRunning () ? 2.0f : 1);

				double fVal = m_PayperSec * fRate * GameManager.Instance.gameInfo.ProgressTime;
				m_textValue.text = fVal.ToString ("F10");
				m_textValue.fontSize = 15;
				GetCoin ();
			}
		}

	}

	public void showSellForButton(){
//		if (bBaught) {
//			if (SellFor.activeSelf)
//				SellFor.SetActive (false);
//			else
//				SellFor.SetActive (true);
//		}
//
		if (bBaught) {
			SellFor.SetActive (true);
			TapImage.SetActive (true);
		}
	}

	public void BuyMiner(int nMinerNumber){
		double cost = GameManager.Instance.gameInfo.miners[nMinerNumber].Cost;

		if (GameManager.Instance.m_player.Cashs >= cost) {
//			double fBTC = cost / GameManager.Instance.m_player.BTC2USD;
//			GameManager.Instance.m_player.Bitcoins -= fBTC;
			GameManager.Instance.m_player.Cashs -= cost;
		} else {
			return;
		}

		GameManager.Instance.gameInfo.positions [m_Number].minerNumber = nMinerNumber+1;

		SetBuyMiner (nMinerNumber);
	}

	public void putMiner(int nMinerNumber){
//		double cost = GameManager.Instance.gameInfo.miners[nMinerNumber].Cost;
		SetBuyMiner (nMinerNumber);
	}

	private void SetBuyMiner(int nMinerNumber){
		m_MinerNumber = nMinerNumber+1;
		m_Cost = GameManager.Instance.gameInfo.miners[nMinerNumber].Cost;
		m_PayperSec = GameManager.Instance.gameInfo.miners [nMinerNumber].PayPerSecond;
		m_ProgressTime = GameManager.Instance.gameInfo.ProgressTime;

		bBaught = true;

		m_buy.SetActive (false);
		string strName = "Miner" + m_MinerNumber + "/Position" + (m_Number+1);
		Texture2D tex = Resources.Load("Sprites/Object/"+strName) as Texture2D;
		if (tex != null) {
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			m_image.GetComponent<Image> ().sprite = sprite;
			m_image.GetComponent<Image> ().sprite.name = "Position" + (m_Number+1);
		}
		m_image.SetActive (true);
		startLoading ();

//		updateUIValues();
		showFan (nMinerNumber+1);
	}

	void showFan(int nMinerNum){
		for (int i = 0; i<m_Fans.Length; i++) {
			string strVal = "Fan" + nMinerNum;
			if(m_Fans[i].name.Equals(strVal))
				m_Fans [i].SetActive (true);
			else
				m_Fans [i].SetActive (false);
		}
	}

	void Reset(){
		string str = "Miner" + m_Number.ToString ();
		PlayerPrefs.SetInt (str, 0);
	}

	public void GetCoin(){
//		if (bLoading)
//			return;
		
		double fRate = (GameManager.Instance.getFanFlying () ? 2.0f : 1);
		fRate *= (GameManager.Instance.getPCRunning () ? 2.0f : 1);

		m_EarnedBTC = m_PayperSec * fRate * GameManager.Instance.gameInfo.ProgressTime;
		startLoading ();
		GameManager.Instance.m_player.Bitcoins += m_EarnedBTC;

//		double fVal = m_EarnedBTC * GameManager.Instance.m_player.BTC2USD;
//		GameManager.Instance.m_player.Cashs += fVal;

		updateUIValues();

		GameManager.Instance.checkGetBlock ();
		//SetScore
		FBManager.Instance.SetScore (GameManager.Instance.m_player.Bitcoins);
	} 

	void startLoading(){
		progressBar.GetComponent<Animator> ().SetTrigger ("startMiner");
//		bLoading = true;

		GameObject icon = m_textValue.transform.parent.Find ("Icon").gameObject;
		Texture2D tex = Resources.Load ("Sprites/UI/iconBTC") as Texture2D;
		Sprite sprite = Sprite.Create (tex, new Rect (0,0,tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		icon.GetComponent<Image> ().sprite = sprite;
		icon.GetComponent<Image> ().sprite.name = "coinIcon";

		//m_time = Time.time;
		//m_textValue.text = "0";
	}

	void stopLoading(){
		progressBar.GetComponent<Animator> ().SetTrigger ("stopMiner");
//		bLoading = false;

		GameObject icon = m_textValue.transform.parent.Find ("Icon").gameObject;
		Texture2D tex = Resources.Load ("Sprites/UI/iconCash") as Texture2D;
		Sprite sprite = Sprite.Create (tex, new Rect (0,0,tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		icon.GetComponent<Image> ().sprite = sprite;
		icon.GetComponent<Image> ().sprite.name = "iconCash";
	}

	void updateUIValues(){
		GameManager.Instance.setTextBitcoins (GameManager.Instance.m_player.Bitcoins.ToString("F10"));
//		GameManager.Instance.setTextCashCoins (GameManager.Instance.m_player.Cashs.ToString("F0"));
	}

	public void SellMiner(){
		double cost = GameManager.Instance.gameInfo.miners[m_MinerNumber-1].Cost;

//		double fBTC = cost / GameManager.Instance.m_player.BTC2USD;
//		GameManager.Instance.m_player.Bitcoins += fBTC*0.8f;
		GameManager.Instance.m_player.Cashs += cost*0.8f;

		GameManager.Instance.gameInfo.positions [m_Number].minerNumber = 0;

		//m_Cost = 0;
		m_Cost = GameManager.Instance.gameInfo.positions[m_Number].Cost;
		m_PayperSec = 0;
		m_ProgressTime = 0;

		bBaught = false;
		stopLoading ();
		m_buy.SetActive (true);
		m_image.GetComponent<Image> ().sprite = null;
		m_image.SetActive (false);
		m_textValue.text = m_Cost.ToString ("F0");
		m_textValue.fontSize = 30;
	}
}
