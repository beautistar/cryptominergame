﻿using System;
using UnityEngine.UI;

[System.Serializable]

public class Ranking{
	public int nOrder;
	public string playerName;
	public double bitcoins;
}
