﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingItem : MonoBehaviour {
	public Image ImageOrder;
	public Text TextOrder;
	public Image ImageAvatar;
	public Text TextUserName;
	public Text TextScore;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
