﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardItem : MonoBehaviour {
	public Image imageSelect;
	public Image imageIcon;
	public Text textValue;
	public Image imageClaimed;

	public int day;
	public float reward;
	public bool isClaimed;
	public bool isAvailable;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setTextValue(string str){
		textValue.text = str;
	}

	public void setImageSelect(){
	}

	public void setImageIcon(Sprite sprite){
		imageIcon.sprite = sprite;
		imageIcon.sprite.name = sprite.name;
	}

	public void Refresh(){
		if (isAvailable) {
			imageSelect.enabled = isAvailable;
		}
		if (isClaimed) {
		}
		imageClaimed.enabled = isClaimed;
	}

	public void InitSelect(){
		isClaimed = false;
		isAvailable = false;
		imageSelect.enabled = false;
	}

	public void ProcessReward(){
		Reward reward = GameManager.Instance.gameInfo.Reward [day - 1];
		int nBlocks = 0;
		double fCash = 0;
		double fPMin = 0;
		double fBTC = 0;

		switch (reward.rewardType) {
		case RewardType.plus:
			nBlocks = GameManager.Instance.m_player.Blocks + reward.nValue;
			fCash = GameManager.Instance.m_player.Cashs + reward.nValue;
//			fPMin = GameManager.Instance.m_player.BTCPMin + reward.nValue;
			fBTC = GameManager.Instance.m_player.Bitcoins + reward.nValue;
			break;
		case RewardType.times:
			nBlocks = GameManager.Instance.m_player.Blocks + GameManager.Instance.m_player.Blocks * reward.nValue;
			fCash = GameManager.Instance.m_player.Cashs + GameManager.Instance.m_player.Cashs* reward.nValue;
//			fPMin = GameManager.Instance.m_player.BTCPMin * reward.nValue;
//			fBTC = GameManager.Instance.m_player.Bitcoins * reward.nValue;
			break;
		case RewardType.hour:
			nBlocks = 0;
			fCash = 0;
			fPMin = GameManager.Instance.m_player.Bitcoins + GameManager.Instance.m_player.BTCPMin * 60.0f * reward.nValue;
			fBTC = 0;
			break;
		case RewardType.percent:
//			nBlocks = (int)(GameManager.Instance.m_player.Blocks * (reward.nValue/100.0f));
			fCash = GameManager.Instance.m_player.Cashs + (int)GameManager.Instance.m_player.Cashs * (reward.nValue/100.0f);
//			fPMin = (int)GameManager.Instance.m_player.BTCPMin * (reward.nValue/100.0f);
//			fBTC = (int)GameManager.Instance.m_player.Bitcoins * (reward.nValue/100.0f);
			break;
		}

		switch (reward.valueType){
		case RewardValueType.Block:
			GameManager.Instance.m_player.Blocks = nBlocks;
			break;
		case RewardValueType.Cash:
			GameManager.Instance.m_player.Cashs = fCash;
			GameManager.Instance.m_player.Bitcoins += fCash / GameManager.Instance.m_player.BTC2USD;
			break;
		case RewardValueType.BTC:
			GameManager.Instance.m_player.Bitcoins = fBTC;
			GameManager.Instance.m_player.Cashs += fBTC * GameManager.Instance.m_player.BTC2USD;
			break;
		case RewardValueType.BTCPMin:
			GameManager.Instance.m_player.Bitcoins = fPMin;
			GameManager.Instance.m_player.Cashs += fPMin * GameManager.Instance.m_player.BTC2USD;
			break;
		}	
	}
}
