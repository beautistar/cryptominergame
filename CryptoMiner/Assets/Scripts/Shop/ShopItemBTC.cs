﻿using System;

public enum BTCUpgradeType{
	increase,
	times,
	plus
}

public class ShopItemBTC:ShopItem
{
	public BTCUpgradeType m_Type;
	public double m_BTCValue;
	public double m_payBTC;

	public ShopItemBTC ()
	{
		itemType = ItemType.bitcoin;
	}

	public override void processShopItem(){
		switch (m_Type) {
		case BTCUpgradeType.increase:
			GameManager.Instance.m_player.Bitcoins += GameManager.Instance.m_player.Bitcoins * m_BTCValue;
			break;
		case BTCUpgradeType.times:
			GameManager.Instance.m_player.Bitcoins *= m_BTCValue;
			break;
		case BTCUpgradeType.plus:
			GameManager.Instance.m_player.Bitcoins += m_BTCValue;
			break;
		}	
		GameManager.Instance.m_player.Bitcoins -= m_payBTC;

//		GameManager.Instance.m_player.Cashs += GameManager.Instance.m_player.Bitcoins * GameManager.Instance.m_player.BTC2USD;
		GameManager.Instance.gameInfo.paywithbtc[index].paid = 1;
	}
}

