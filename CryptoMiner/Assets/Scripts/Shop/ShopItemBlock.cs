﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class ShopItemBlock : ShopItem
{
	public float m_Time;
	public int m_Blocks;

	private const string LAST_REWARD_TIME = "LastRewardTime";
	private const string FMT = "O";

	public ShopItemBlock ()
	{
		itemType = ItemType.block;
	}

	public override void processShopItem(){
		//float fVal = m_Time * 3600;

		string lastClaimedTimeStr = PlayerPrefs.GetString(LAST_REWARD_TIME);
		DateTime lastRewardTime = DateTime.ParseExact(lastClaimedTimeStr, FMT, CultureInfo.InvariantCulture);
		DateTime newTime = RewardManager.Instance.timer.AddHours (m_Time);
		TimeSpan difference = (lastRewardTime-newTime).Add(new TimeSpan (0, 24, 0, 0));
		if (difference.TotalSeconds <= 0) {
			
		}
//		string lastClaimedStr = newTime.ToString(FMT);
//		PlayerPrefs.SetString(LAST_REWARD_TIME, lastClaimedStr);
			
		RewardManager.Instance.timer = RewardManager.Instance.timer.AddHours (m_Time);

		GameManager.Instance.m_player.PlayingTime.AddHours(m_Time);
		GameManager.Instance.m_player.Blocks -= m_Blocks;
		double fPMin = GameManager.Instance.m_player.Bitcoins + GameManager.Instance.m_player.BTCPMin * 60.0f * m_Time;
		GameManager.Instance.m_player.Bitcoins = fPMin;
//		GameManager.Instance.m_player.Cashs += fPMin * GameManager.Instance.m_player.BTC2USD;
		GameManager.Instance.gameInfo.paywithblock[index].paid = 1;
	}
}

