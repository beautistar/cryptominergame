﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopItemButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void selectShopItem(){
		ShopItem shopItem = transform.GetComponent<ShopItem> ();
		shopItem.processShopItem ();
		hideShopPanel();
	}

	void hideShopPanel(){
		ShopManager.Instance.hideShopPanel ();
	}

}
