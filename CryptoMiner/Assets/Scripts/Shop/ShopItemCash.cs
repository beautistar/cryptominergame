﻿using System;
public class ShopItemCash:ShopItem
{
	public double m_BTC;
	public int m_Cash;

	public ShopItemCash ()
	{
		itemType = ItemType.cash;
	}

	public override void processShopItem(){
		GameManager.Instance.m_player.Bitcoins *= m_BTC;
//		GameManager.Instance.m_player.Cashs += GameManager.Instance.m_player.Bitcoins * GameManager.Instance.m_player.BTC2USD;
		GameManager.Instance.m_player.Cashs -= m_Cash;
		GameManager.Instance.gameInfo.paywithcash[index].paid = 1;
	}
}

