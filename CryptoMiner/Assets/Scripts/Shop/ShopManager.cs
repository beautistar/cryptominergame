﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShopManager : MonoBehaviour {
	private static ShopManager _instance = null;
	public static ShopManager Instance {get{ return _instance;}}

	public GameObject btnBlock, btnCash, btnBitcoin;

	public Texture2D[] blockSprites;
	public Texture2D[] cashSprites;
	public Texture2D[] bitcoinSprites;

	public GameObject content;

	void Awake(){
		_instance = this;
		clickBlockButton ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void setBlockButton(bool bflag){
		string strName = "Sprites/UI/btnBlockOn";

		if(!bflag)
			strName = "Sprites/UI/btnBlockOff";

		Texture2D tex = Resources.Load (strName) as Texture2D;
		Sprite sprite = Sprite.Create (tex, new Rect (0,0,tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		btnBlock.GetComponent<Image> ().sprite = sprite;
	}

	private void setCashButton(bool bflag){
		string strName = "Sprites/UI/btnCashOn";

		if(!bflag)
			strName = "Sprites/UI/btnCashOff";

		Texture2D tex = Resources.Load (strName) as Texture2D;
		Sprite sprite = Sprite.Create (tex, new Rect (0,0,tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		btnCash.GetComponent<Image> ().sprite = sprite;
	}

	private void setBitcoinButton(bool bflag){
		string strName = "Sprites/UI/btnBitcoinOn";

		if(!bflag)
			strName = "Sprites/UI/btnBitcoinOff";

		Texture2D tex = Resources.Load (strName) as Texture2D;
		Sprite sprite = Sprite.Create (tex, new Rect (0,0,tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		btnBitcoin.GetComponent<Image> ().sprite = sprite;
	}

	private void setButtons(bool flag){
		setBlockButton(flag);
		setCashButton(flag);
		setBitcoinButton(flag);
	}

	private void loadItems(Texture2D[] texs){		
		for (int i = 0; i < texs.Length; i++) {
			GameObject obj = GameObject.Instantiate(Resources.Load("Prefabs/ShopItem")) as GameObject;
			if (texs [i] == null)
				continue;
			Sprite sprite = Sprite.Create (texs[i], new Rect (0, 0, texs[i].width, texs[i].height), new Vector2 (0.5f, 0.5f));
			obj.GetComponent<Image> ().sprite = sprite;
			obj.GetComponent<Image> ().sprite.name = texs [i].name;
			obj.transform.SetParent (content.transform);
			obj.name = "ShopItem" + i;
			obj.transform.localScale = Vector3.one;
			obj.transform.GetComponent<RectTransform> ().localPosition = Vector3.zero;
		}
	}
	void removeAllItems(){
		for (int i=content.transform.childCount-1;i>=0;i--) {
			Transform child = content.transform.GetChild (i);
			child.SetParent( null );
			GameObject.Destroy(child.gameObject);
		}
	}

	public void clickBlockButton(){
		removeAllItems ();
		setButtons (false);
		setBlockButton (true);
		int j = 0;
		blockSprites = new Texture2D[GameManager.Instance.gameInfo.paywithblock.Length];

		for (int i = 0; i < GameManager.Instance.gameInfo.paywithblock.Length; i++) {
			string texname = GameManager.Instance.gameInfo.paywithblock [i].name;
			Texture2D tex = Resources.Load ("Sprites/UI/"+texname) as Texture2D;
			if (tex == null) {
				tex = Resources.Load ("Sprites/UI/"+"PayBlock_4h") as Texture2D;
			}
			blockSprites [j++] = tex;
		}

		loadItems (blockSprites);
		for (int i = 0; i < GameManager.Instance.gameInfo.paywithblock.Length; i++) {
			GameObject shopItem = content.transform.GetChild (i).gameObject;
			ShopItemBlock shopItemBlock =  shopItem.AddComponent<ShopItemBlock> ();
			shopItemBlock.index = i;
			shopItemBlock.m_Time = GameManager.Instance.gameInfo.paywithblock [i].hours;
			shopItemBlock.m_Blocks = GameManager.Instance.gameInfo.paywithblock [i].blocks;

			Button button = shopItem.GetComponent<Button> ();
//			SetButtonOnClickListener (button);
//			button.onClick.AddListener (() => hideShopPanel());

			if (GameManager.Instance.gameInfo.paywithblock [i].blocks > GameManager.Instance.m_player.Blocks ||
				GameManager.Instance.gameInfo.paywithblock [i].paid == 1) {
//				shopItem.GetComponent<Image> ().color = new Color32 (180, 180, 180, 180);
				button.interactable = false;
			}

			Image iconImage = shopItem.transform.Find ("Icon").GetComponent<Image> () as Image;
			Texture2D tex = Resources.Load ("Sprites/UI/"+"iconBlock") as Texture2D;
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			iconImage.sprite = sprite;
			iconImage.name = tex.name;
			Text valueText = shopItem.transform.Find ("Value").GetComponent<Text> () as Text;
			valueText.text = GameManager.Instance.gameInfo.paywithblock [i].blocks.ToString();
		}
	}

	void SetButtonOnClickListener(Button button){
		button.onClick.AddListener (delegate {
			hideShopPanel();	
		});
	}

	public void clickCashButton(){
		removeAllItems ();
		setButtons (false);
		setCashButton (true);
		int j = 0;
		cashSprites = new Texture2D[GameManager.Instance.gameInfo.paywithcash.Length];

		for (int i = 0; i < GameManager.Instance.gameInfo.paywithcash.Length; i++) {
			string texname = GameManager.Instance.gameInfo.paywithcash [i].name;
			Texture2D tex = Resources.Load ("Sprites/UI/"+texname) as Texture2D;
			if (tex == null)
				tex = Resources.Load ("Sprites/UI/"+"PayCash_BandWidth") as Texture2D;
			cashSprites [j++] = tex;
		}
		loadItems (cashSprites);

		for (int i = 0; i < GameManager.Instance.gameInfo.paywithcash.Length; i++) {
			GameObject shopItem = content.transform.GetChild (i).gameObject;
			ShopItemCash shopItemCash =  shopItem.AddComponent<ShopItemCash> ();
			shopItemCash.index = i;
			shopItemCash.m_BTC = GameManager.Instance.gameInfo.paywithcash [i].btc;
			shopItemCash.m_Cash = GameManager.Instance.gameInfo.paywithcash [i].cost;

			Button button = shopItem.GetComponent<Button> ();

			if (GameManager.Instance.gameInfo.paywithcash [i].cost > GameManager.Instance.m_player.Cashs||
				GameManager.Instance.gameInfo.paywithcash [i].paid == 1) {
//				shopItem.GetComponent<Image> ().color = new Color32 (180, 180, 180, 180);
				button.interactable = false;
			}

			Image iconImage = shopItem.transform.Find ("Icon").GetComponent<Image> () as Image;
			Texture2D tex = Resources.Load ("Sprites/UI/"+"iconCash") as Texture2D;
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			iconImage.sprite = sprite;
			iconImage.name = tex.name;
			Text valueText = shopItem.transform.Find ("Value").GetComponent<Text> () as Text;
			valueText.text = GameManager.Instance.gameInfo.paywithcash [i].cost.ToString();
		}
	}

	public void clickBitcoinButton(){
		removeAllItems ();
		setButtons (false);
		setBitcoinButton (true);
		int j = 0;
		bitcoinSprites = new Texture2D[GameManager.Instance.gameInfo.paywithbtc.Length];

		for (int i = 0; i < GameManager.Instance.gameInfo.paywithbtc.Length; i++) {
			string texname = GameManager.Instance.gameInfo.paywithbtc[i].name;
			Texture2D tex = Resources.Load ("Sprites/UI/"+texname) as Texture2D;
			if (tex == null)
				tex = Resources.Load ("Sprites/UI/"+"PayBTC_Hardware") as Texture2D;
			bitcoinSprites [j++] = tex;
		}
		loadItems (bitcoinSprites);

		for (int i = 0; i < GameManager.Instance.gameInfo.paywithbtc.Length; i++) {
			GameObject shopItem = content.transform.GetChild (i).gameObject;
			ShopItemBTC shopItemBTC =  shopItem.AddComponent<ShopItemBTC> ();
			shopItemBTC.index = i;
			shopItemBTC.m_Type = GameManager.Instance.gameInfo.paywithbtc [i].type;
			shopItemBTC.m_BTCValue = GameManager.Instance.gameInfo.paywithbtc [i].value;
			shopItemBTC.m_payBTC = GameManager.Instance.gameInfo.paywithbtc [i].btc;

			Button button = shopItem.GetComponent<Button> ();
			if (GameManager.Instance.gameInfo.paywithbtc [i].btc> GameManager.Instance.m_player.Bitcoins||
				GameManager.Instance.gameInfo.paywithbtc [i].paid == 1) {
//				shopItem.GetComponent<Image> ().color = new Color32 (180, 180, 180, 180);
				button.interactable = false;
			}

			Image iconImage = shopItem.transform.Find ("Icon").GetComponent<Image> () as Image;
			Texture2D tex = Resources.Load ("Sprites/UI/"+"iconBTC") as Texture2D;
			Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
			iconImage.sprite = sprite;
			iconImage.name = tex.name;
			Text valueText = shopItem.transform.Find ("Value").GetComponent<Text> () as Text;
			valueText.text = GameManager.Instance.gameInfo.paywithbtc[i].btc.ToString();
		}
	}

	void enableShopItem(){
		
	}

	public void hideShopPanel(){
		removeAllItems ();
		transform.gameObject.SetActive (false);
	}

	public void showShopPanel(){
		clickBlockButton ();
		transform.gameObject.SetActive (true);
	}
}
