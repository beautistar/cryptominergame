﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#pragma warning disable 618
public class VungleAdsManager : MonoBehaviour {
	private static VungleAdsManager _instance = null;
	public static VungleAdsManager Instance {get { return _instance;}}

	string actionName = "";

	#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WSA_10_0 || UNITY_WINRT_8_1 || UNITY_METRO
	void Start(){
		actionName = "";
		Vungle.init( "59e785e4a81a003c1700e9f9", "59b000ec6413db5d6d00253b", "59b000ec6413db5d6d00253b" );
	}

	void Awake(){
		_instance = this;
	}

	public void DisplayAds(string strName){
		actionName = strName;
		if (Vungle.isAdvertAvailable ()){
			DisplayInsentivizedAds ();
			//Vungle.playAd ();
		}
		else
			CancelAction ();
	}

	public void DisplayInsentivizedAds(){
		Vungle.playAd( true, "user-tag", 6 );
	}

	#region Optional: Example of Subscribing to All Events
	void OnEnable()
	{
		Vungle.onAdStartedEvent += onAdStartedEvent;
		Vungle.onAdFinishedEvent += onAdFinishedEvent;
		Vungle.adPlayableEvent += adPlayableEvent;
	}


	void OnDisable()
	{
		Vungle.onAdStartedEvent -= onAdStartedEvent;
		Vungle.onAdFinishedEvent -= onAdFinishedEvent;
		Vungle.adPlayableEvent -= adPlayableEvent;
	}


	void onAdStartedEvent()
	{
		Debug.Log( "onAdStartedEvent" );
	}


	void onAdFinishedEvent(AdFinishedEventArgs arg)
	{
		Debug.Log("onAdFinishedEvent. watched: " + arg.TimeWatched + ", length: " + arg.TotalDuration  + ", isCompletedView: " + arg.IsCompletedView);

		ProgressAction ();
	}

	void ProgressAction(){
		switch (actionName) {
		case "WallFan":
			Transform Widding = GameManager.Instance.WallFan.transform.parent.transform.Find ("Widding").transform;
			Widding.GetComponent<WallFan> ().startFan ();
			break;
		case "PC":
			GameManager.Instance.setPCRunning (true);
			GameManager.Instance.showPCTimer (true);
			break;
		case "WhileGone":
			GameManager.Instance.showWhileGoneScreen (false);
			GameManager.Instance.updateWhileGoneValue ();
			break;
		}
	}

	void CancelAction(){
		switch (actionName) {
		case "WallFan":
			Transform Widding = GameManager.Instance.WallFan.transform.parent.transform.Find ("Widding").transform;
			Widding.GetComponent<WallFan> ().cancelFan();
			break;
		case "PC":
			GameManager.Instance.cancelPCRunning ();
			GameManager.Instance.showPCTimer (false);
			break;
		case "WhileGone":
			GameManager.Instance.showWhileGoneScreen (false);
			break;
		}
	}

	void adPlayableEvent(bool playable)
	{
		Debug.Log( "adPlayableEvent: " + playable);
	}

	#endregion


	#endif
}
